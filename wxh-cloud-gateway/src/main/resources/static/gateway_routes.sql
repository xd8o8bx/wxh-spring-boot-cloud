/*
 Navicat Premium Data Transfer

 Source Server         : 开发82
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : 10.249.12.80:3306
 Source Schema         : wxhtest

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 11/11/2020 17:11:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gateway_routes
-- ----------------------------
DROP TABLE IF EXISTS `gateway_routes`;
CREATE TABLE `gateway_routes`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `route_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由id',
  `route_uri` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '转发目标uri',
  `route_order` int(11) NULL DEFAULT NULL COMMENT '路由执行顺序',
  `predicates` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '断言字符串集合，字符串结构：[{\r\n                \"name\":\"Path\",\r\n                \"args\":{\r\n                   \"pattern\" : \"/zy/**\"\r\n                }\r\n              }]',
  `filters` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '过滤器字符串集合，字符串结构：{\r\n              	\"name\":\"StripPrefix\",\r\n              	 \"args\":{\r\n              	 	\"_genkey_0\":\"1\"\r\n              	 }\r\n              }',
  `is_ebl` tinyint(1) NULL DEFAULT NULL COMMENT '是否启用',
  `is_del` tinyint(1) NULL DEFAULT NULL COMMENT '是否删除',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gateway_routes
-- ----------------------------
INSERT INTO `gateway_routes` VALUES (1, 'server-order', 'lb://server-order', 0, '[{\n	\"name\": \"Path\",\n	\"args\": {\n		\"pattern\": \"/order/**\"\n	}\n}]', '[{\n	\"name\": \"StripPrefix\",\n	\"args\": {\n		\"parts\": \"1\"\n	}\n},\n{\n	\"name\": \"RequestRateLimiter\",\n	\"args\": {\n		\"key-resolver\": \"#{@hostAddrKeyResolver}\",\n		\"redis-rate-limiter.replenishRate\": \"1\",\n		\"redis-rate-limiter.burstCapacity\": \"2\"\n	}\n},\n{\n	\"name\": \"Hystrix\",\n	\"args\": {\n		\"name\": \"fallbackcmd\",\n		\"fallbackUri\": \"forward:/fallback\"\n	}\n}]', 0, 0, '2020-11-03 16:47:58', '2020-11-11 16:23:40');
INSERT INTO `gateway_routes` VALUES (2, 'server-stock', 'lb://server-stock', 0, '[{\r\n	\"name\": \"Path\",\r\n	\"args\": {\r\n		\"pattern\": \"/stock/**\"\r\n	}\r\n}]', '[{\n	\"name\": \"StripPrefix\",\n	\"args\": {\n		\"parts\": \"1\"\n	}\n},\n{\n	\"name\": \"RequestRateLimiter\",\n	\"args\": {\n		\"key-resolver\": \"#{@hostAddrKeyResolver}\",\n		\"redis-rate-limiter.replenishRate\": \"100\",\n		\"redis-rate-limiter.burstCapacity\": \"200\"\n	}\n},\n{\n	\"name\": \"Hystrix\",\n	\"args\": {\n		\"name\": \"fallbackcmd\",\n		\"fallbackUri\": \"forward:/fallback\"\n	}\n}]', 0, 0, '2020-11-03 16:54:08', '2020-11-11 15:01:44');

SET FOREIGN_KEY_CHECKS = 1;
