package com.wxh.cloud.gateway.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wxh.cloud.core.PageQueryParam;
import com.wxh.cloud.core.ResultDto;
import com.wxh.cloud.gateway.config.RedisConfig;
import com.wxh.cloud.gateway.dto.GatewayRouteDefinition;
import com.wxh.cloud.gateway.model.GatewayRoutes;
import com.wxh.cloud.gateway.service.RoutesService;
import com.wxh.cloud.gateway.service.impl.DynamicRouteHelper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("sys/gatewayroutes")
public class GatewayRoutesController {

    @Autowired private RoutesService routesService;
    @Autowired private StringRedisTemplate redisTemplate;
    @Autowired private DynamicRouteHelper dynamicRouteHelper;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @ResponseBody
    public ResultDto list(@RequestBody PageQueryParam queryParam){
        Page<GatewayRoutes> page = routesService.queryPage(queryParam);
        return ResultDto.success(page);
    }
    
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public ResultDto info(@PathVariable("id") Long id){
        GatewayRoutes gatewayRoutes = routesService.getById(id);
        return ResultDto.success(gatewayRoutes);
    }
    
    /**
     * 保存
     */
    @RequestMapping("/save")
    public ResultDto save(@RequestBody GatewayRoutes gatewayRoutes){
    	routesService.add(gatewayRoutes);
        return ResultDto.success(gatewayRoutes);
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public ResultDto update(@RequestBody GatewayRoutes gatewayRoutes){
    	routesService.update(gatewayRoutes);
        return ResultDto.success();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public ResultDto delete(@RequestBody Long[] ids){
    	for (int i = 0; i < ids.length; i++) {
    		routesService.delete(ids[i] , true);
		}
        return ResultDto.success();
    }
    
    /**
     * 获取所有动态路由信息
     * @return
     */
    @RequestMapping("/routes")
    @ResponseBody
    public String getRouteDefinitions(){
        //先从redis中取，再从mysql中取
        String result = redisTemplate.opsForValue().get(RedisConfig.routeKey);
        if(!StringUtils.isEmpty(result)){
            System.out.println("返回 redis 中的路由信息......");
        }else{
            System.out.println("返回 mysql 中的路由信息......");
            result = JSON.toJSONString(routesService.getRouteDefinitions());
            //再set到redis
            redisTemplate.opsForValue().set(RedisConfig.routeKey , result);
        }
        System.out.println("路由信息：" + result);
        return result;
    }
    
    @RequestMapping("/reload")
    @ResponseBody
    public ResultDto reload(){

    	dynamicRouteHelper.reload();
    	return ResultDto.success("重新加载成功！");
    }
}
