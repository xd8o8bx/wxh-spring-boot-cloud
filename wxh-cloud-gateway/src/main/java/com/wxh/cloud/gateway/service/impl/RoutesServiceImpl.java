package com.wxh.cloud.gateway.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxh.cloud.core.PageQueryParam;
import com.wxh.cloud.gateway.dao.GatewayRoutesDao;
import com.wxh.cloud.gateway.dto.GatewayRouteDefinition;
import com.wxh.cloud.gateway.model.GatewayRoutes;
import com.wxh.cloud.gateway.service.RoutesService;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RoutesServiceImpl extends ServiceImpl<GatewayRoutesDao, GatewayRoutes> implements RoutesService {
    
    @Override
    public int add(GatewayRoutes route) {
        route.setIsEbl(false);
        route.setIsDel(false);
        route.setCreateTime(new Date());
        route.setUpdateTime(new Date());
        return baseMapper.insertSelective(route);
    }

    @Override
    public int update(GatewayRoutes route) {
        route.setUpdateTime(new Date());
        return baseMapper.updateByPrimaryKeySelective(route);
    }

    @Override
    public int delete(Long id, boolean isDel) {
        return baseMapper.deleteByPrimaryKey(id , isDel);
    }

    @Override
    public int enableById(Long id, boolean isEbl) {
        return baseMapper.enableById(id , isEbl);
    }

    @Override
    public GatewayRoutes getById(Long id) {
        return baseMapper.selectByPrimaryKey(id);
    }

    /**
     * 查询路由信息
     * @return
     */
    @Override
    public List<GatewayRoutes> getRoutes(GatewayRoutes route) {
        return baseMapper.getRoutes(route);
    }

    /**
     * 返回组装后网关需要的路由信息
     * @return
     */
    @Override
    public List<GatewayRouteDefinition> getRouteDefinitions() {
        List<GatewayRouteDefinition> routeDefinitions = new ArrayList<>();
        GatewayRoutes route = new GatewayRoutes();
        route.setIsDel(false);
        route.setIsEbl(false);
        List<GatewayRoutes> routes = getRoutes(route);
        for(GatewayRoutes gatewayRoute : routes){
            GatewayRouteDefinition routeDefinition = new GatewayRouteDefinition();
            routeDefinition.setId(gatewayRoute.getRouteId());
            routeDefinition.setUri(gatewayRoute.getRouteUri());
            routeDefinition.setFilters(gatewayRoute.getFilterDefinition());
            routeDefinition.setPredicates(gatewayRoute.getPredicateDefinition());
            routeDefinitions.add(routeDefinition);
        }
        return routeDefinitions;
    }
    
    @Override
    public Page<GatewayRoutes> queryPage(PageQueryParam queryParam) {
        Page<GatewayRoutes> page = new Page<GatewayRoutes>(queryParam.getCurrent(), queryParam.getSize(), true);
        QueryWrapper<GatewayRoutes> wrapper = new QueryWrapper<GatewayRoutes>();
        
        if (!StringUtils.isEmpty(queryParam.getQuerydata())) {
            GatewayRoutes queryBean = JSONObject.parseObject(queryParam.getQuerydata(), GatewayRoutes.class);
            if (!StringUtils.isEmpty(queryBean.getRouteId())) {
            	wrapper.likeRight("route_id", queryBean.getRouteId());
			}
            
            if (!StringUtils.isEmpty(queryBean.getRouteUri())) {
            	wrapper.likeRight("route_uri", queryBean.getRouteUri());
			}
		}
        wrapper.eq("is_del", false);
        
        return super.page(page, wrapper);
    }
}
