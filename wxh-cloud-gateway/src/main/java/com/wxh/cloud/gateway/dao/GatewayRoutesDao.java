package com.wxh.cloud.gateway.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wxh.cloud.gateway.model.GatewayRoutes;

import java.util.List;

@Mapper
public interface GatewayRoutesDao  extends BaseMapper<GatewayRoutes> {
    int deleteByPrimaryKey(@Param("id") Long id ,@Param("isDel") boolean isDel);

    int enableById(@Param("id") Long id,@Param("isEbl") boolean isEbl);

    int insert(GatewayRoutes record);

    int insertSelective(GatewayRoutes record);

    GatewayRoutes selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(GatewayRoutes record);

    int updateByPrimaryKey(GatewayRoutes record);

    List<GatewayRoutes> getRoutes(GatewayRoutes route);
}