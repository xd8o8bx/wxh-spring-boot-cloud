package com.wxh.cloud.gateway.resolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

/**
 * 根据Hostname进行限流  KeyResolver 只能存在一个
 * @author WXH
 */
@Component
public class HostAddrKeyResolver implements KeyResolver {
	
	private  static Logger logger = LoggerFactory.getLogger(HostAddrKeyResolver.class);
	
	@Override
	public Mono<String> resolve(ServerWebExchange exchange) {
		//logger.info("进入Hostname进行限流"+exchange.getRequest().getRemoteAddress().getAddress().getHostAddress());
		return  Mono.just(exchange.getRequest().getRemoteAddress().getAddress().getHostAddress());
	}

}
