package com.wxh.cloud.gateway.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.wxh.cloud.core.ResultDto;

/**
 * 异常处理器
 * 
 */
@RestControllerAdvice
public class GatewayExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 处理自定义异常
	 */
	@ExceptionHandler(GatewayException.class)
	public ResultDto handleRRException(GatewayException e){
		if (e.getData()!=null) {
			return ResultDto.error(e.getCode(),e.getMessage(),e.getData());
		}
		return ResultDto.error(e.getCode(),e.getMessage());
	}

	@ExceptionHandler(Exception.class)
	public ResultDto handleException(Exception e){
		logger.error(e.getMessage(), e);
		return ResultDto.error();
	}
}
