package com.wxh.cloud.gateway.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wxh.cloud.gateway.model.DynamicVersion;

@Mapper
public interface DynamicVersionDao  extends BaseMapper<DynamicVersion> {
    int deleteByPrimaryKey(Long id);

    int insert(DynamicVersion record);

    int insertSelective(DynamicVersion record);

    DynamicVersion selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(DynamicVersion record);

    int updateByPrimaryKey(DynamicVersion record);

    //获取最后一次发布的版本号
    Long getLastVersion();

    //获取所有的版本发布信息
    List<DynamicVersion> listAll();
}