package com.wxh.cloud.gateway.service;


import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxh.cloud.gateway.model.DynamicVersion;

public interface DynamicVersionService extends IService<DynamicVersion> {

    int add(DynamicVersion version);

    int update(DynamicVersion version);

    int delete(Long id);

    /**
     * 获取最后一次发布的版本号
     * @return
     */
    Long getLastVersion();

    //获取所有的版本发布信息
    List<DynamicVersion> listAll();
}
