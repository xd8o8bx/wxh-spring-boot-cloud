package com.wxh.cloud.gateway.common;

import com.wxh.cloud.core.BaseException;

public class GatewayException extends BaseException{

	private static final long serialVersionUID = 1L;

    public GatewayException(String msg) {
		super(msg);
	}
    public GatewayException(String msg,Object data) {
		super(msg,data);
	}
	
	public GatewayException(String msg, Throwable e) {
		super(msg, e);
	}
	
	public GatewayException(String msg, int code) {
		super(msg,code);
	}
	
	public GatewayException(String msg, int code, Throwable e) {
		super(msg,code, e);
	}
	
	public GatewayException(String msg, int code,Object data) {
		super(msg,code,data);
	}
	public GatewayException(String msg, int code,Object data, Throwable e) {
		super(msg,code,data, e);
	}
}
