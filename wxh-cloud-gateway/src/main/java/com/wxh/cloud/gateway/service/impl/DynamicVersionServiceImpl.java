package com.wxh.cloud.gateway.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxh.cloud.gateway.config.RedisConfig;
import com.wxh.cloud.gateway.dao.DynamicVersionDao;
import com.wxh.cloud.gateway.model.DynamicVersion;
import com.wxh.cloud.gateway.service.DynamicVersionService;
import com.wxh.cloud.gateway.service.RoutesService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class DynamicVersionServiceImpl  extends ServiceImpl<DynamicVersionDao, DynamicVersion> implements DynamicVersionService{

    @Autowired private StringRedisTemplate redisTemplate;
    @Autowired private RoutesService routesService;

    @Override
    public int add(DynamicVersion version) {
        version.setCreateTime(new Date());
        int result = baseMapper.insertSelective(version);

        //发布时，把版本信息与路由信息存入redis
        redisTemplate.opsForValue().set(RedisConfig.versionKey , String.valueOf(version.getId()));
        redisTemplate.opsForValue().set(RedisConfig.routeKey , JSON.toJSONString(routesService.getRouteDefinitions()));

        return result;
    }

    @Override
    public int update(DynamicVersion version) {
        return baseMapper.updateByPrimaryKeySelective(version);
    }

    @Override
    public int delete(Long id) {
        return baseMapper.deleteByPrimaryKey(id);
    }

    /**
     * 获取最后一次发布的版本号
     * @return
     */
    @Override
    public Long getLastVersion() {
        return baseMapper.getLastVersion();
    }

    @Override
    public List<DynamicVersion> listAll() {
        return baseMapper.listAll();
    }
}
