package com.wxh.cloud.gateway;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.wxh.cloud.core.ResultDto;
import com.wxh.cloud.gateway.service.impl.DynamicRouteHelper;

@RestController
@EnableScheduling
@EnableDiscoveryClient
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class GatewayApplication  implements CommandLineRunner  {

	private  static Logger logger = LoggerFactory.getLogger(GatewayApplication.class);
	
	@Autowired private DynamicRouteHelper dynamicRouteHelper;
	
	public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
	
	@Override
	public void run(String... args) throws Exception {
		logger.info("网关服务启动完成,机器IP："+InetAddress.getLocalHost().getHostAddress());
		
		dynamicRouteHelper.reload();
		
		logger.info("全量加载路由完成！");
	}
	
	@RequestMapping("/")
	@ResponseBody
    public String index(){
        return "这是网关服务！";
    }

	@RequestMapping("/health")
    @ResponseBody
    String home() {
        return "这是网关服务！";
    }
	
    @RequestMapping("/fallback")
    @ResponseBody
    public ResultDto defaultfallback(){
    	logger.warn("降级操作...");
        return ResultDto.error("服务暂时不可用");
    }
}