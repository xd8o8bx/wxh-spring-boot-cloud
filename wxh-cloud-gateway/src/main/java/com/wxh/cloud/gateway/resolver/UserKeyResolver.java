package com.wxh.cloud.gateway.resolver;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.web.server.ServerWebExchange;

import reactor.core.publisher.Mono;

/**
 * 根据用户限流   KeyResolver 只能存在一个
 * @author WXH
 */
public class UserKeyResolver implements KeyResolver {

	@Override
	public Mono<String> resolve(ServerWebExchange exchange) {
		 return Mono.just(exchange.getRequest().getQueryParams().getFirst("user"));
	}

}
