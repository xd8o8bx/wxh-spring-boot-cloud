package com.wxh.cloud.gateway.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wxh.cloud.core.ResultDto;

/**
 * 系统菜单
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController{

	private static String  menu = "[{\r\n" + 
			"	\"menuId\": 1,\r\n" + 
			"	\"parentId\": 0,\r\n" + 
			"	\"parentName\": null,\r\n" + 
			"	\"name\": \"系统管理\",\r\n" + 
			"	\"url\": \"\",\r\n" + 
			"	\"perms\": \"\",\r\n" + 
			"	\"type\": 0,\r\n" + 
			"	\"icon\": \"system\",\r\n" + 
			"	\"orderNum\": 0,\r\n" + 
			"	\"open\": null,\r\n" + 
			"	\"list\": [{\r\n" + 
			"		\"menuId\": 2,\r\n" + 
			"		\"parentId\": 1,\r\n" + 
			"		\"parentName\": null,\r\n" + 
			"		\"name\": \"管理员列表\",\r\n" + 
			"		\"url\": \"sys/gatewayroutes\",\r\n" + 
			"		\"perms\": null,\r\n" + 
			"		\"type\": 1,\r\n" + 
			"		\"icon\": \"admin\",\r\n" + 
			"		\"orderNum\": 1,\r\n" + 
			"		\"open\": null,\r\n" + 
			"		\"list\": null\r\n" + 
			"	},\r\n" + 
			"	{\r\n" + 
			"		\"menuId\": 41,\r\n" + 
			"		\"parentId\": 1,\r\n" + 
			"		\"parentName\": null,\r\n" + 
			"		\"name\": \"字典项管理\",\r\n" + 
			"		\"url\": \"sys/sysdict\",\r\n" + 
			"		\"perms\": \"\",\r\n" + 
			"		\"type\": 1,\r\n" + 
			"		\"icon\": \"zidianguanl\",\r\n" + 
			"		\"orderNum\": 8,\r\n" + 
			"		\"open\": null,\r\n" + 
			"		\"list\": null\r\n" + 
			"	}]\r\n" + 
			"}]";

	/**
	 * 导航菜单
	 */
	@RequestMapping("/nav")
	public ResultDto nav(){
		JSONObject jb = new JSONObject();
			jb.put("menuList", JSONArray.parse(menu));
			jb.put("permissions", new HashSet<String>());
		return ResultDto.success(jb);
	}
	
	/**
	 * 所有菜单列表
	 */
//	@RequestMapping("/list")
//	@RequiresPermissions("sys:menu:list")
//	public ResultDto list(){
//		List<SysMenuEntity> menuList = sysMenuService.selectList(null);
//		for(SysMenuEntity sysMenuEntity : menuList){
//			SysMenuEntity parentMenuEntity = sysMenuService.selectById(sysMenuEntity.getParentId());
//			if(parentMenuEntity != null){
//				sysMenuEntity.setParentName(parentMenuEntity.getName());
//			}
//		}
//
//		return ResultDto.success(menuList);
//	}
	
	/**
	 * 选择菜单(添加、修改菜单)
	 */
//	@RequestMapping("/select")
//	@RequiresPermissions("sys:menu:select")
//	public ResultDto select(){
//		//查询列表数据
//		List<SysMenuEntity> menuList = sysMenuService.queryNotButtonList();
//		
//		//添加顶级菜单
//		SysMenuEntity root = new SysMenuEntity();
//		root.setMenuId(0L);
//		root.setName("一级菜单");
//		root.setParentId(-1L);
//		root.setOpen(true);
//		menuList.add(root);
//		
//		return ResultDto.success(menuList);
//	}
	
	/**
	 * 菜单信息
	 */
//	@RequestMapping("/info/{menuId}")
//	@RequiresPermissions("sys:menu:info")
//	public ResultDto info(@PathVariable("menuId") Long menuId){
//		SysMenuEntity menu = sysMenuService.selectById(menuId);
//		return ResultDto.success(menu);
//	}
	
	/**
	 * 保存
	 */
//	@SysLog("保存菜单")
//	@PostMapping("/save")
//	@RequiresPermissions("sys:menu:save")
//	public ResultDto save(SysMenuEntity menu){
//		//数据校验
//		verifyForm(menu);
//		
//		sysMenuService.insert(menu);
//		
//		return ResultDto.success();
//	}
	
	/**
	 * 修改
	 */
//	@SysLog("修改菜单")
//	@RequestMapping("/update")
//	@RequiresPermissions("sys:menu:update")
//	public ResultDto update(SysMenuEntity menu){
//		//数据校验
//		verifyForm(menu);
//		
//		sysMenuService.updateAllColumnById(menu);
//		
//		return ResultDto.success();
//	}
	
	/**
	 * 删除
	 */
//	@SysLog("删除菜单")
//	@RequestMapping("/delete/{menuId}")
//	@RequiresPermissions("sys:menu:delete")
//	public ResultDto delete(@PathVariable("menuId") Long menuId){
//		if(menuId <= 31){
//			return ResultDto.error("系统菜单，不能删除");
//		}
//
//		//判断是否有子菜单或按钮
//		List<SysMenuEntity> menuList = sysMenuService.queryListParentId(menuId);
//		if(menuList.size() > 0){
//			return ResultDto.error("请先删除子菜单或按钮");
//		}
//
//		sysMenuService.delete(menuId);
//
//		return ResultDto.success();
//	}
	
	/**
	 * 验证参数是否正确
	 */
//	private void verifyForm(SysMenuEntity menu){
//		if(StringUtils.isBlank(menu.getName())){
//			throw new LogisticsException("菜单名称不能为空");
//		}
//		
//		if(menu.getParentId() == null){
//			throw new LogisticsException("上级菜单不能为空");
//		}
//		
//		//菜单
//		if(menu.getType() == Constant.MenuType.MENU.getValue()){
//			if(StringUtils.isBlank(menu.getUrl())){
//				throw new LogisticsException("菜单URL不能为空");
//			}
//		}
//		
//		//上级菜单类型
//		int parentType = Constant.MenuType.CATALOG.getValue();
//		if(menu.getParentId() != 0){
//			SysMenuEntity parentMenu = sysMenuService.selectById(menu.getParentId());
//			parentType = parentMenu.getType();
//		}
//		
//		//目录、菜单
//		if(menu.getType() == Constant.MenuType.CATALOG.getValue() ||
//				menu.getType() == Constant.MenuType.MENU.getValue()){
//			if(parentType != Constant.MenuType.CATALOG.getValue()){
//				throw new LogisticsException("上级菜单只能为目录类型");
//			}
//			return ;
//		}
//		
//		//按钮
//		if(menu.getType() == Constant.MenuType.BUTTON.getValue()){
//			if(parentType != Constant.MenuType.MENU.getValue()){
//				throw new LogisticsException("上级菜单只能为菜单类型");
//			}
//			return ;
//		}
//	}
}
