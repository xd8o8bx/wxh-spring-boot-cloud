package com.wxh.cloud.gateway.config;

import org.springframework.cloud.consul.discovery.ConsulDiscoveryProperties;
import org.springframework.cloud.consul.discovery.HeartbeatProperties;
import org.springframework.cloud.consul.discovery.TtlScheduler;
import org.springframework.cloud.consul.serviceregistry.ConsulRegistration;
import org.springframework.cloud.consul.serviceregistry.ConsulServiceRegistry;

import com.ecwid.consul.v1.ConsulClient;
/**
 * 解决单节点多实例注册问题
 * @author WXH
 *
 */
//@Configuration
public class MyConsulServiceRegistry extends ConsulServiceRegistry {

	/**
     * 参照源码定义声名
     */
//	@Autowired(required = false)
//    private TtlScheduler ttlScheduler;
	
	public MyConsulServiceRegistry(ConsulClient client, ConsulDiscoveryProperties properties, TtlScheduler ttlScheduler,
			HeartbeatProperties heartbeatProperties) {
		super(client, properties, ttlScheduler, heartbeatProperties);
		// TODO Auto-generated constructor stub
	}
	
	@Override
    public void register(ConsulRegistration reg) {
		//重写注册的ID
        reg.getService().setId(reg.getService().getId()+"-"+reg.getService().getAddress());
        super.register(reg);
    }

}
