package com.wxh.cloud.gateway.service;


import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wxh.cloud.core.PageQueryParam;
import com.wxh.cloud.gateway.dto.GatewayRouteDefinition;
import com.wxh.cloud.gateway.model.GatewayRoutes;

public interface RoutesService  extends IService<GatewayRoutes> {

    int add(GatewayRoutes route);

    int update(GatewayRoutes route);

    int delete(Long id , boolean isDel);

    int enableById(Long id , boolean isEbl);

    GatewayRoutes getById(Long id);

    /**
     * 查询路由信息
     * @return
     */
    List<GatewayRoutes> getRoutes(GatewayRoutes route);

    /**
     * 返回组装后网关需要的路由信息
     * @return
     */
    List<GatewayRouteDefinition> getRouteDefinitions();

	Page<GatewayRoutes> queryPage(PageQueryParam queryParam);
}
