package com.wxh.cloud.order.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxh.cloud.order.client.model.OrderInfo;

public interface OrderInfoService  extends IService<OrderInfo> {

	public Object getAll();
	
}
