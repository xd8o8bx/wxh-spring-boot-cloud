package com.wxh.cloud.stock.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WebMvc配置
 */
@Configuration
public class WebConfig implements WebMvcConfigurer  {
	
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
            .allowedOrigins("*")
            .allowCredentials(true)
            .allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
            .maxAge(3600);
    }
	
//    @Override
//    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
//        MappingJackson2HttpMessageConverter jackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
//        ObjectMapper objectMapper = jackson2HttpMessageConverter.getObjectMapper();
//
//        //生成json时，将所有Long转换成String
//        SimpleModule simpleModule = new SimpleModule();
//        simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
//        simpleModule.addSerializer(Long.TYPE, ToStringSerializer.instance);
//        objectMapper.registerModule(simpleModule);
//
//        jackson2HttpMessageConverter.setObjectMapper(objectMapper);
//        converters.add(0, jackson2HttpMessageConverter);
//    }
	/**
     * 解决JSON返回实体类报错
     */
	@Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		/** 是否通过请求Url的扩展名来决定media type */
        configurer.favorPathExtension(false);
        /** 不检查Accept请求头 */
        //.ignoreAcceptHeader(true)
        //.parameterName("mediaType")
        /** 设置默认的media yype */
        //.defaultContentType(MediaType.TEXT_HTML)
        /** 请求以.html结尾的会被当成MediaType.TEXT_HTML */
        //.mediaType("html", MediaType.TEXT_HTML)
        /** 请求以.json结尾的会被当成MediaType.APPLICATION_JSON */
        //.mediaType("json", MediaType.APPLICATION_JSON);
    }


    /**
     * 文件上传大小限制配置
     * @return
     */
//    @Bean
//    public MultipartConfigElement multipartConfigElement() {
//        MultipartConfigFactory factory = new MultipartConfigFactory();
//        //单个文件最大
//        factory.setMaxFileSize("102400KB"); //KB,MB
//        /// 设置总上传数据总大小
//        factory.setMaxRequestSize("102400KB");
//        return factory.createMultipartConfig();
//    }
}
