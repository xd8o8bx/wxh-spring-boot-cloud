package com.wxh.cloud.stock.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxh.cloud.stock.client.model.StoreInfo;
import com.wxh.cloud.stock.client.service.StoreInfoService;
import com.wxh.cloud.stock.dao.StoreInfoDao;

@Service
public class StoreInfoServiceImpl  extends ServiceImpl<StoreInfoDao, StoreInfo>  implements StoreInfoService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	@DS("slave")
	public Object getAll() {
		return super.list();
	}
}
