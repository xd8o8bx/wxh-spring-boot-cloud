package com.wxh.cloud.stock.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wxh.cloud.stock.client.model.StoreInfo;

@Mapper
public interface StoreInfoDao  extends BaseMapper<StoreInfo>{

}
