package com.wxh.cloud.stock.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wxh.cloud.core.ResultDto;
import com.wxh.cloud.stock.client.service.StoreInfoService;

@RestController
@RequestMapping("store/")
public class TestApi {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	 @Autowired 
	 private StoreInfoService stockInfoService;
	 
	 @RequestMapping(path = {"all"},method = RequestMethod.GET)
	 public ResultDto allOrder() {
		 logger.info("*****************************************************");
//		 try {
//			Thread.sleep(4000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		Long start = System.currentTimeMillis(); 
		 
		stockInfoService.list();
		 
		
		System.out.println("请求耗时："+(System.currentTimeMillis()-start));
		
		return ResultDto.success(stockInfoService.getAll());
	 }

}
