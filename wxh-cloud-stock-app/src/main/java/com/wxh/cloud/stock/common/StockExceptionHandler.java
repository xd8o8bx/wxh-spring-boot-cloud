package com.wxh.cloud.stock.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.wxh.cloud.core.BaseExceptionEnum;
import com.wxh.cloud.core.ResultDto;

/**
 * 异常处理器
 * 
 */
@RestControllerAdvice
public class StockExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 处理自定义异常
	 */
	@ExceptionHandler(StockException.class)
	public ResultDto handleRRException(StockException e){
		if (e.getData()!=null) {
			return ResultDto.error(e.getCode(),e.getMessage(),e.getData());
		}
		return ResultDto.error(e.getCode(),e.getMessage());
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	public ResultDto handlerNoFoundException(Exception e) {
		return ResultDto.error(BaseExceptionEnum.NO_HANDLER_FOUND.getCode(), BaseExceptionEnum.NO_HANDLER_FOUND.getMessage());
	}

	@ExceptionHandler(DuplicateKeyException.class)
	public ResultDto handleDuplicateKeyException(DuplicateKeyException e){
		logger.error(e.getMessage(), e);
		return ResultDto.error("数据库中已存在该记录");
	}

//	@ExceptionHandler(AuthorizationException.class)
//	public ResultDto handleAuthorizationException(AuthorizationException e){
//		logger.error(e.getMessage(), e);
//		return ResultDto.error("没有权限，请联系管理员授权");
//	}

	@ExceptionHandler(Exception.class)
	public ResultDto handleException(Exception e){
		logger.error(e.getMessage(), e);
		return ResultDto.error();
	}
}
