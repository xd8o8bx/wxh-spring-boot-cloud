package com.wxh.cloud.stock;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
public class StockApplication implements CommandLineRunner  {

	private  static Logger logger = LoggerFactory.getLogger(StockApplication.class);
	
	public static void main(String[] args) {
        SpringApplication.run(StockApplication.class, args);
    }
	
	@Override
	public void run(String... args) throws Exception {
		logger.info("库存服务启动完成,机器IP："+InetAddress.getLocalHost().getHostAddress());
	}

	@RequestMapping("/health")
    @ResponseBody
    String home() {
        return "这是库存服务！";
    }
}