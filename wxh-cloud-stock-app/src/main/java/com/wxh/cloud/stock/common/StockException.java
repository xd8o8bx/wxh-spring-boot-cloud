package com.wxh.cloud.stock.common;

import com.wxh.cloud.core.BaseException;

public class StockException extends BaseException{

	private static final long serialVersionUID = 1L;

    public StockException(String msg) {
		super(msg);
	}
    public StockException(String msg,Object data) {
		super(msg,data);
	}
	
	public StockException(String msg, Throwable e) {
		super(msg, e);
	}
	
	public StockException(String msg, int code) {
		super(msg,code);
	}
	
	public StockException(String msg, int code, Throwable e) {
		super(msg,code, e);
	}
	
	public StockException(String msg, int code,Object data) {
		super(msg,code,data);
	}
	public StockException(String msg, int code,Object data, Throwable e) {
		super(msg,code,data, e);
	}
}
