package com.wxh.cloud.stock.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wxh.cloud.stock.client.model.StoreInfo;

public interface StoreInfoService extends IService<StoreInfo> {

	public Object getAll();
}
