package com.wxh.cloud.stock.client.model;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("store_info")
public class StoreInfo implements Serializable{

	private static final long serialVersionUID = 1L;

	@TableId
	private Long id;
	
	@TableField("item_code")
	private String itemCode;
	
	@TableField("store")
	private int store;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public int getStore() {
		return store;
	}

	public void setStore(int store) {
		this.store = store;
	}

}
