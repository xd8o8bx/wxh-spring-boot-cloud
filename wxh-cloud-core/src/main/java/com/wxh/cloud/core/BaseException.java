package com.wxh.cloud.core;

public class BaseException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
    private String msg;
    private int code = 500;
    private Object data;
    
    public BaseException(String msg) {
		super(msg);
		this.msg = msg;
	}
    public BaseException(String msg,Object data) {
		super(msg);
		this.msg = msg;
		this.data = data;
	}
	
	public BaseException(String msg, Throwable e) {
		super(msg, e);
		this.msg = msg;
	}
	
	public BaseException(String msg, int code) {
		super(msg);
		this.msg = msg;
		this.code = code;
	}
	
	public BaseException(String msg, int code, Throwable e) {
		super(msg, e);
		this.msg = msg;
		this.code = code;
	}
	
	public BaseException(String msg, int code,Object data) {
		super(msg);
		this.msg = msg;
		this.code = code;
		this.data = data;
	}
	public BaseException(String msg, int code,Object data, Throwable e) {
		super(msg, e);
		this.msg = msg;
		this.code = code;
		this.data = data;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
