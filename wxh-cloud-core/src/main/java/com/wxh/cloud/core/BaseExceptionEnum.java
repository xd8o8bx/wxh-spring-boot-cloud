package com.wxh.cloud.core;

public enum BaseExceptionEnum {

	/**
	 * 服务器内部错误
	 */
	SERVER_ERROR(500, "服务器异常"),

	/**
	 *  ********************************  100 系列  ****************************************
	 */
	
	/**
	 * token异常
	 */
	TOKEN_EXPIRED(101, "token过期"), 
	TOKEN_ERROR(102, "token验证失败"),

	/**
	 * 签名异常
	 */
	SIGN_ERROR(103, "签名验证失败"),

	/**
	 * jwt鉴权认证 帐号密码错误
	 */
	AUTH_REQUEST_ERROR(104, "鉴权账号密码错误"),
	
	/**
	 *  ********************************  200 系列  ****************************************
	 */
	
	/**
	 * 参数非空校验失败
	 */
	PARAM_NOT_NULL(201, "必填参数为空"),
	
	SOURCE_NOT_FOUND(202, "获取不到相应的资源"),

	ILLEGAL_OPERATION(203, "不支持此操作"),
	
	/**
	 *  ********************************  400 系列  ****************************************
	 */
	NO_HANDLER_FOUND(404, "路径不存在，请检查路径是否正确");

	BaseExceptionEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	private int code;

	private String message;

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
