package com.wxh.cloud.core;

import java.io.Serializable;

/**
 * 操作成功，数据传输对象
 * 
 */
public class ResultDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private int code = 0;
	private String message = "success";
	private Object data;
	
	public ResultDto() {
		super();
	}

	public ResultDto(Object data) {
		this.code = 0;
		this.message = "success";
		this.data = data;
	}
	
	public ResultDto(Object data,String message) {
		this.message = message;
		this.data = data;
	}
	public ResultDto(String message) {
		this.message = message;
	}
	public ResultDto(int code,String message) {
		this.code = code;
		this.message = message;
	}
	public ResultDto(int code,String message,Object data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public ResultDto(Object data,int code,String message) {
		this.data = data;
		this.code = code;
		this.message = message;
	}
	/*----------------ERROR-----------------------*/
	public static ResultDto error() {
		return error(BaseExceptionEnum.SERVER_ERROR.getCode(), "未知异常，请联系管理员");
	}
	
	public static ResultDto error(String message) {
		return error(BaseExceptionEnum.SERVER_ERROR.getCode(), message);	
	}
	
	public static ResultDto error(int code, String message) {
		return new ResultDto(code, message);
	}
	
	public static ResultDto error(int code, String message,Object data) {
		return new ResultDto(code, message,data);
	}
	
	/*----------------SUCCESS-----------------------*/
	public static ResultDto success() {
		return new ResultDto();
	}
	
	public static ResultDto success(String message) {
		return new ResultDto(message);
	}
	
	public static ResultDto success(Object data) {
		return new ResultDto(data);
	}
	
	public static ResultDto success(Object data,String message) {
		return new ResultDto(data,message);
	}

	public int getCode() {
		return code;
	}
 
	public void setCode(int code) {
		this.code = code;
	}
 
	public String getMessage() {
		return message;
	}
 
	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
 
}
