package com.wxh.cloud.core;

import java.io.Serializable;

/**
 * 分页列表查询对象
 * @author WXH
 */
public class PageQueryParam implements Serializable{

	private static final long serialVersionUID = 1L;

	private int current;
	
	private int size;
	
	private String orderByField;
	
	private boolean asc;
	
	private String querydata;//查询条件

	public int getCurrent() {
		return current;
	}

	public void setCurrent(int current) {
		this.current = current;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getOrderByField() {
		return orderByField;
	}

	public void setOrderByField(String orderByField) {
		this.orderByField = orderByField;
	}
 
	public boolean isAsc() {
		return asc;
	}

	public void setAsc(boolean asc) {
		this.asc = asc;
	}

	public String getQuerydata() {
		return querydata;
	}

	public void setQuerydata(String querydata) {
		this.querydata = querydata;
	}

}
