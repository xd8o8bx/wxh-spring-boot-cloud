package com.wxh.cloud.order.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.consul.discovery.ConsulDiscoveryProperties;
import org.springframework.cloud.consul.discovery.HeartbeatProperties;
import org.springframework.cloud.consul.discovery.TtlScheduler;
import org.springframework.cloud.consul.serviceregistry.ConsulServiceRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;
import com.ecwid.consul.v1.ConsulClient;

@Configuration
public class CloudConfig {

	@Autowired(required = false)
	private TtlScheduler ttlScheduler;
	
	@Bean
	@Primary
	public ConsulServiceRegistry consulServiceRegistry(ConsulClient consulClient, ConsulDiscoveryProperties properties,
			HeartbeatProperties heartbeatProperties) {
		return new MyConsulServiceRegistry(consulClient, properties, ttlScheduler, heartbeatProperties);
	}
	
   //@LoadBalanced
   // @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
	
	
}
