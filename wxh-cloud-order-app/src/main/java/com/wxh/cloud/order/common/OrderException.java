package com.wxh.cloud.order.common;

import com.wxh.cloud.core.BaseException;

public class OrderException extends BaseException{

	private static final long serialVersionUID = 1L;

    public OrderException(String msg) {
		super(msg);
	}
    public OrderException(String msg,Object data) {
		super(msg,data);
	}
	
	public OrderException(String msg, Throwable e) {
		super(msg, e);
	}
	
	public OrderException(String msg, int code) {
		super(msg,code);
	}
	
	public OrderException(String msg, int code, Throwable e) {
		super(msg,code, e);
	}
	
	public OrderException(String msg, int code,Object data) {
		super(msg,code,data);
	}
	public OrderException(String msg, int code,Object data, Throwable e) {
		super(msg,code,data, e);
	}
}
