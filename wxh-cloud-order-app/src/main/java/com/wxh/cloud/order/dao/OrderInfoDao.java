package com.wxh.cloud.order.dao;

import org.apache.ibatis.annotations.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wxh.cloud.order.client.model.OrderInfo;

@Mapper
public interface OrderInfoDao  extends BaseMapper<OrderInfo>{

}
