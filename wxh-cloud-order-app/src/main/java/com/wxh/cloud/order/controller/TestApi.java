package com.wxh.cloud.order.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wxh.cloud.core.ResultDto;
import com.wxh.cloud.order.client.service.OrderInfoService;

@RestController
@RequestMapping("order/")
public class TestApi {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	 @Autowired 
	 private OrderInfoService orderInfoService;
	 
	 @RequestMapping(path = {"all"},method = RequestMethod.GET)
	 public ResultDto allOrder() {
		 
		 logger.info("*****************************************************");
		 
		 orderInfoService.list();
		 return ResultDto.success(orderInfoService.getAll());
	 }

}
