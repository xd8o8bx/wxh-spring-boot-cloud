package com.wxh.cloud.order.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wxh.cloud.order.client.model.OrderInfo;
import com.wxh.cloud.order.client.service.OrderInfoService;
import com.wxh.cloud.order.dao.OrderInfoDao;

@Service
public class OrderInfoServiceImpl  extends ServiceImpl<OrderInfoDao, OrderInfo>  implements OrderInfoService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	@DS("slave")
	public Object getAll() {
		return super.list();
	}
}
