package com.wxh.cloud.order;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class OrderApplication  implements CommandLineRunner  {

	private  static Logger logger = LoggerFactory.getLogger(OrderApplication.class);
	
	public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }
	
	@Override
	public void run(String... args) throws Exception {
		logger.info("订单服务启动完成,机器IP："+InetAddress.getLocalHost().getHostAddress());
	}

	@RequestMapping("/health")
    @ResponseBody
    String home() {
        return "这是订单服务！";
    }
}