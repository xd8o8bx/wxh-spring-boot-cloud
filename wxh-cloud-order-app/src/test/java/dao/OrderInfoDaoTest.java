package dao;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.wxh.cloud.order.client.model.OrderInfo;
import com.wxh.cloud.order.dao.OrderInfoDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderInfoDaoTest {

	@Autowired
    private OrderInfoDao orderInfoDao;

    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        List<OrderInfo> userList = orderInfoDao.selectList(null);
        //Assert.assertEquals(5, userList.size());
        userList.forEach(System.out::println);
    }
	
}
